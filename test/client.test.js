/* eslint-env mocha */
'use strict'

const assert = require('assert')
const PackageHunterClient = require('../src/client')
describe('Client Test', () => {
  describe('_sessionCookie Test', () => {
    const cookieName = 'AWSALBCORS'
    const cookieValue = 'sessionCookie'

    const host = 'https://example.com'

    const opts = {
      cookieName
    }

    let client
    beforeEach(() => {
      client = new PackageHunterClient(host, opts)
    })

    it('can retrieve the session cookie from an array of cookie headers in the response', () => {
      const cookies = [
        null,
        'some-header=implementation',
        `${cookieName}=${cookieValue}`
      ]

      const resp = {
        headers: {
          'set-cookie': cookies
        }
      }

      const sessionCookie = client._sessionCookie(resp)

      assert.strictEqual(sessionCookie.key, cookieName)
      assert.strictEqual(sessionCookie.value, cookieValue)
    })

    it('can retrieve the session cookie if it is the only cookie in the response', () => {
      const resp = {
        headers: {
          'set-cookie': `${cookieName}=${cookieValue}`
        }
      }

      const sessionCookie = client._sessionCookie(resp)

      assert.strictEqual(sessionCookie.key, cookieName)
      assert.strictEqual(sessionCookie.value, cookieValue)
    })
  })
})
