workflow:
  rules:
    # For merge requests, create a pipeline.
    - if: '$CI_MERGE_REQUEST_IID'
    # For `main` branch, create a pipeline (this includes on schedules, pushes, merges, etc.).
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    # For tags, create a pipeline.
    - if: '$CI_COMMIT_TAG'

default:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.7.2-git-2.29-lfs-2.9-node-12.18-yarn-1.22-graphicsmagick-1.3.34
  tags:
    - gitlab-org
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - .npm/

stages:
- test
- build
- release

include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml
  - local: '/ci/template/Package-Hunter.gitlab-ci.yml'

unit-test:
  stage: test
  script: 
    - npm install
    - npm test
  rules:
    # For releases on the default branch, don't create a pipeline
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE =~ /^chore\(release\):/
      when: never
    - if: $CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED
      when: never
    - when: on_success

lint-ci-template:
  stage: test
  image: registry.gitlab.com/gitlab-org/security-products/package-hunter/pipelinecomponents/yamllint:amd64-0.19.3@sha256:2ae7c4419707adadaecb81b4748ddf61390e0029816553726fb330a3fafe9b6b
  script:
    - yamllint ci/template/Package-Hunter.gitlab-ci.yml
  rules:
    # For releases on the default branch, don't create a pipeline
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE =~ /^chore\(release\):/
      when: never
    - if: $CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED
      when: never
    - when: on_success

lint-commit-msg:
  stage: test
  script:
    - npm install
    - '[[ -z "${CI_MERGE_REQUEST_DIFF_BASE_SHA}" ]] || npx commitlint --from $CI_MERGE_REQUEST_DIFF_BASE_SHA --to HEAD --verbose'

# Test that the ci template can be included and is free of syntax errors.
# This job is not expected to succeed because
# the cli doesn't support npm projects at the moment.
# The job is expected to fail with 401 Unauthorized.
package_hunter-yarn:
  # to test changes, we need to run the job if the template is modified
  rules:
    # For releases on the default branch, don't create a pipeline
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE =~ /^chore\(release\):/
      when: never
    - if: $CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED
      when: never
    - changes:
      - ci/template/Package-Hunter.gitlab-ci.yml
  variables:
    PACKAGE_HUNTER_HOST: "https://api.package-hunter.xyz"
  after_script: 
    - cat $CI_PROJECT_DIR/gl-dependency-scanning-report.json

build-image:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - echo $CI_COMMIT_TAG
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE:latest --destination $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  rules:
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE =~ /^chore\(release\):/
    when: never
  - if: $CI_COMMIT_TAG && $CI_COMMIT_REF_PROTECTED

.semantic-release:
  image: node:16.16.0-buster-slim@sha256:34a3b4b7c2704ce52372c42b9b7945b41fe8eb98d74e8af472add0b599b6e1ff
  stage: release
  before_script:
    - apt-get update && apt-get install -y --no-install-recommends git-core ca-certificates
    - npm ci
  script:
    - npx semantic-release $DRY_RUN_OPT -b $CI_COMMIT_REF_NAME

publish:
  extends: .semantic-release
  variables:
    GITLAB_TOKEN: $GITLAB_TOKEN
  rules:
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE =~ /^chore\(release\):/
    when: never
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
